# voltage-graphs: Directed graphs with edges invertibly labelled by group elements

Voltage graphs (also called gain graphs and labelled quotient graphs)
are useful building blocks of finite lattice graphs,
because they separate translation from structure of unit cell.

This package contains two classes (`Voltage` and `Derivable`),
simple Map-based implementation, example and utility function `unfold`.

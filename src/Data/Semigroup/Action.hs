{-# LANGUAGE MultiParamTypeClasses #-}
module Data.Semigroup.Action where

import Data.Semigroup (Semigroup)

-- | Class for semigroups acting on some type.
-- Law: 'act x . act y = act (x <> y)'
class Semigroup s => SemigroupAction s a where
  act :: s -> a -> a

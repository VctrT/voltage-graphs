{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TypeFamilies           #-}
module Graph.Voltage.Class where

import Data.Group
import Data.Semigroup.Action
import GHC.Exts              (Constraint)

-- | Voltage graphs - graphs with edges labelled with group elements.
class Voltage vg where
  -- | Invert the edge and its group element.
  -- 'invert . invert = id'
  invert :: Group y
         => y
         -- ^ group element at edge
         -> (v, v)
         -- ^ edge
         -> vg y v -> vg y v

-- | Voltage graphs which can be unfolded into usual finite digraph.
class Voltage vg => Derivable vg g | vg -> g where
  -- | Make so called derived graph. Type is intentionally weakened
  derive :: (Semigroup y, SemigroupAction y v)
         => [y]
         -- ^ List of elements which will act on voltage graph vertices. These can be for example all elements of translations' subgroup for space group 'y'
         -> vg y v
         -- ^ Voltage graph
         -> g v
         -- ^ Unfolded digraph. When type 'y' is group and provided list contains all elements of this group,
         -- this digraph contain edges in both directions, and so can be made undirected

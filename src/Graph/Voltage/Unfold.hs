module Graph.Voltage.Unfold(unfold) where

import Data.Function      ((&))
import Data.List.NonEmpty (NonEmpty, fromList)
import Data.Semigroup     (sconcat, stimes)
import Numeric.Natural

-- | "Unfold" a list of generators (usually of finite Abelian subgroup of some group).
-- Takes list of generators with their maximum multiplicities '[(n_1, g_1), ... (n_k, g_k)]',
-- and returns list of all possible products of the form g_1^i_1 g_2^i_2 ... g_k^i_k, where 0 < i_1 <= n_1, ... 0 < i_k <= n_k.
-- Type of this function is intentionally weakened to enable more interesting applications
-- like lattices with open boundary conditions, which require only operations of 'Semigroup'.
-- Condition of generator's power 'i_j > 0' means that in this case "derived graph" will not contain initial unit cell.
-- This makes no problems when there's property 'g_i^n_i = 1'.
unfold :: Semigroup g => [(Natural, g)] -> [g]
unfold xs =
  let (ns, ss) = unzip xs
  in map (\n -> [1 .. n]) ns
     & sequence
     & map ( \l -> zip [0 .. ] l
             & map (\(ix, times) -> stimes times (ss !! ix))
             & fromList
             & sconcat
           )

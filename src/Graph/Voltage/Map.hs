{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE TypeFamilies          #-}
module Graph.Voltage.Map where

import Data.Function ((&))
import Data.Maybe    (fromMaybe)
import Prelude       hiding (reverse)

import Lens.Micro    (over, _1, _2)

import qualified Data.Group            as G
import           Data.Semigroup.Action

import           Data.Map (Map)
import qualified Data.Map as Map
import           Data.Set (Set)
import qualified Data.Set as Set

import qualified Graph.Voltage.Class as C

-- | Map-based directed graph.
newtype DiGraph w v = DiGraph { getDiGraph :: Map (v, v) w }
  deriving(Show, Eq)

-- | Map-based voltage graph: directed hypergraph with edges labelled with group elements and optionally with some additional labels.
-- 'Ord' constaint is pulled in to avoid adding constraints to classes 'Voltage' and 'Derivable'.
data VGraph e g v where
  VGraph :: forall e g v.
           (Ord v, Ord g)
         => { getVG :: Map (v, v) (Map g e) }
         -> VGraph e g v

deriving instance (Show g, Show v, Show e) => Show (VGraph e g v)
deriving instance (Eq g, Eq v, Eq e) => Eq (VGraph e g v)

-- | Make directed edge for unweighted voltage graph
edge :: (Ord v, Ord g) => g -> v -> v -> VGraph () g v
edge g i j = VGraph $ Map.singleton (i, j) (Map.singleton g ())

edgeW :: (Ord v, Ord g) => g -> e -> v -> v -> VGraph e g v
edgeW g e i j = VGraph $ Map.singleton (i, j) (Map.singleton g e)

-- | Make undirected edge (labelled by identity element).
-- edgeU :: (Ord v, Ord g, Monoid g) => v -> v -> VGraph g v ()
edgeU i j = VGraph $ Map.singleton (i, j) (Map.singleton mempty ())

edgeUW i j e = VGraph $ Map.singleton (i, j) (Map.singleton mempty e)

-- what to do when edges overlap? we could use (<>) for edges too
instance (Ord v, Ord g) => Semigroup (VGraph e g v) where
  -- x <> y = VGraph (Map.unionWith Map.union (getVG x) (getVG y))
  (VGraph m1) <> (VGraph m2) = VGraph (Map.unionWith Map.union m1 m2)

instance (Ord v, Ord g) => Monoid (VGraph e g v) where
  mempty = VGraph Map.empty

instance C.Voltage (VGraph e) where
  invert g e (VGraph m) = VGraph $ Map.adjust f e m
    where
      f gm = case Map.lookup g gm of
        Nothing -> gm
        Just e -> Map.insert (G.invert g) e $ Map.delete g gm

instance C.Derivable (VGraph e) (DiGraph e) where
  -- for each labelled edge ((i, j), z)
  -- and each operation 'y' from 'ys'
  -- act on i by y
  -- and act on j by (y <> z)
  derive ys (VGraph m) = DiGraph $ Map.fromList $ do
    (edge, gm) <- Map.toList m
    y <- ys
    (z, e) <- Map.toList gm
    let pair = over _1 (act y) . over _2 (act (y <> z)) $ edge
    pure (pair, e)

{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
module Graph.Voltage.Example where

import Data.Proxy
import GHC.TypeLits
import Numeric.Natural

import Data.Group (Group, invert)

import Data.Semigroup.Action

import Graph.Voltage.Class  (derive)
import Graph.Voltage.Map
import Graph.Voltage.Unfold (unfold)

-- | Example: generating finite one-dimensional lattices with periodic boundary conditions

-- | One-dimensional translation parametrized by total number of elementary cells
newtype T (n :: Nat) = T Int
  deriving(Eq, Ord, Show)

instance KnownNat n => Semigroup (T n) where
  T x <> T y = T $ (x + y) `mod` (fromIntegral $ natVal (Proxy :: Proxy n))

instance KnownNat n => SemigroupAction (T n) (Int, Int) where
  act (T t) (localIx, cellIx) = (localIx, cellIx + t)

instance KnownNat n => Monoid (T n) where
  mempty = T 0

instance KnownNat n => Group (T n) where
  invert (T x) = T (negate x)

-- | Voltage graph with translations as edge labels and tuples (localIndex, cellIndex)
type ExampleVG n = VGraph () (T n) (Int, Int)

-- | Voltage graph of chain formed from triangles sharing single vertex.
triangleChainVG :: forall (n :: Nat). KnownNat n => ExampleVG n
triangleChainVG
  = edgeU (0, 0) (1, 0)
  <> edge (T 1) (0, 0) (1, 0)
  <> edge (T 1) (1, 0) (1, 0)

-- | Make triangle chain from voltage graph.
makeTriangleChain :: forall (n :: Nat).
                    KnownNat n
                  => Proxy n
                  -> DiGraph () (Int, Int)
makeTriangleChain p =
  let n = natVal p
      l = fromIntegral n
  in derive (unfold [(l, T 1)]) (triangleChainVG @n)
